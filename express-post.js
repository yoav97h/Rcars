var express = require('express')
var bodyParser = require('body-parser')
var pg = require('pg');
require('dotenv').config();

// Create a new instance of express
var app = express()

app.use(bodyParser.json());
// Tell express to use the body-parser middleware and to not parse extended bodies
app.use(bodyParser.urlencoded({ extended: false }))

var connectionString = process.env.DATABASE_URL;

var client = new pg.Client({
    user: "jagvfqcyjllodu",
    password: "75f552a2e411d54ad363f3a8e39f6e23c47e5a78c8a9f258916941a23cccdb13",
    database: "de827p7nmate6n",
    port: 5432,
    host: "ec2-54-235-80-137.compute-1.amazonaws.com",
    ssl: true
}); 

var port = process.env.PORT || 3000;

client.connect()

function updateComing (coming, day, place) {
    return client.query(`update points 
    set points = points + ((select ${place} from join_map_key where day = ($2))::integer) 
    where name = ANY ($1);`
    , [coming, day]);
}

function updateDriving (driving, day, place) {
    return client.query(`update points 
    set points = points + ((select ${place} from drive_map_key where day = ($2))::integer) 
    where name = ANY ($1);`
    , [driving, day]);
}

function reverseComingPoints (coming, place, day) {
    return client.query(`update points 
    set points = points - ((select ${place} from join_map_key where day = ($2))::integer) 
    where name = ANY ($1);`
    , [coming, day]);
}

function reverseDrivingPoints (driving, place, day) {
    return client.query(`update points 
    set points = points - ((select ${place} from drive_map_key where day = ($2))::integer) 
    where name = ANY ($1);`
    , [driving, day]);
}

function insertHistory (coming, day, place, driving) {
    return client.query(`INSERT INTO 
    events(coming, date, place, driving)
    VALUES 
    ($1,$2,$3,$4);`
    , [coming, day, place, driving]);
}

function deleteEvent(id) {
    return client.query(`DELETE FROM events
    WHERE id= ($1);`
    , [id]);
}

function choose (coming, driverNum) {
    return client.query(`select name 
    from points 
    where name = ANY($1) 
    order by points asc 
    fetch first ($2) rows only`
    ,[coming, driverNum])
}


// Route that receives a POST request to /posts
app.post('/posts', function (req, res) {    
  var body = req.body
  let driverNum = (parseInt(body.coming.length/5) + 
                        (body.coming.length%5 > 0 ? 1 : 0 ));
  if(body.driving.length<driverNum){
        driverNum = driverNum-body.driving.length;
        choose(body.coming, driverNum).then(data => {
            console.log({driving:data.rows})
            let driving;
            driving = data.rows.map((row)=>{
                return row.name;
            })
            driving = driving.concat(body.driving)
            console.log('bodyodood' , body.coming, body.day, body.place, driving)
            insertHistory(body.coming, body.day, body.place, driving).then((data)=>{
                console.log('historyyyyyyyyyy', data)
            }).catch((err)=>{
                console.log('ERORRRORORO', err)
            })
            //Handle removing the drivers from the coming array
            updateComing(body.coming, body.day, body.place).then((data)=>{
                console.log('data coming', data)
            }).catch((err)=>{
                console.log('ERORRRORORO', err)
            })
            updateDriving(driving, body.day, body.place).then((data)=>{
                console.log('data driving', data)
            }).catch((err)=>{
                console.log('ERORRRORORO', err)
            })
            res.json({driving})
        })
  } else {
        insertHistory(body.coming, body.day, body.place, body.driving).then((data)=>{
                console.log('historyyyyyyyyyy', data)
            }).catch((err)=>{
                console.log('ERORRRORORO', err)
            })
            //Handle removing the drivers from the coming array
            updateComing(body.coming, body.day, body.place).then((data)=>{
                console.log('data coming', data)
            }).catch((err)=>{
                console.log('ERORRRORORO', err)
            })
            updateDriving(body.driving, body.day, body.place).then((data)=>{
                console.log('data driving', data)
            }).catch((err)=>{
                console.log('ERORRRORORO', err)
            })
            let resp = body.driving
            res.json({resp})
  }
})

app.get('/events', function (req, res) {
    res.set('Content-Type', 'text/plain')
    getAll().then((data)=>{
        console.log(data)
        let events;
        events = data.rows.map((row)=>{
            return row;
        })
        console.log(events)
        let response = res.send(events)
    })
  })

  app.post('/delete', function (req, res) {
      var body = req.body
      deleteEvent(body.id)
      reverseComingPoints(body.coming, body.place, body.day)
      reverseDrivingPoints(body.driving, body.place, body.day)
      let resp = body.id
      res.json({resp})
  })

  function getAll () {
    return client.query('select * from events');
}

// Tell our app to listen on port 3000
app.listen(port, function (err) {
 if (err) {
     console.log(err)
 }
 console.log(port, 'Server started on port')
})